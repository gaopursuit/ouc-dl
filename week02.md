# 【第二周】卷积神经网络

<br>

# 1、视频学习 

学习专知课程《卷积神经⽹络》，主要内容包括： 

● CNN的基本结构：卷积、池化、全连接 

● 典型的⽹络结构：AlexNet、VGG、GoogleNet、ResNet

<br>

# 2、代码练习

需要使⽤⾕歌的 Colab ，⼤家有任何问题可以随时在群⾥ AT 我。有部分同学已经做过这部分代 

码练习，可以略过。 

- MNIST 数据集分类：构建简单的CNN对 mnist 数据集进⾏分类。同时，还会在实验中学习池化与卷积操作的基本作⽤，  [实验指导链接](https://gitee.com/gaopursuit/ouc-dl/blob/master/lab/week02_ConvNet.ipynb)
- CIFAR10 数据集分类：使⽤ CNN 对 CIFAR10 数据集进⾏分类， [实验指导链接](https://gitee.com/gaopursuit/ouc-dl/blob/master/lab/week02_CNN_CIFAR10.ipynb)
- 使⽤ VGG16 对 CIFAR10 分类，[实验指导链接](https://gitee.com/gaopursuit/ouc-dl/blob/master/lab/week02_VGG_CIFAR10.ipynb)

【第⼀部分：代码练习】在⾕歌 Colab 上完成上⾯的代码练习，关键步骤截图，并附⼀些⾃⼰的 

想法和解读。 

【第⼆部分：问题总结】思考下⾯的问题： 

- dataloader ⾥⾯ shuffle 取不同值有什么区别？ 
- transform ⾥，取了不同值，这个有什么区别？ 
- epoch 和 batch 的区别？ 
- 1x1的卷积和 FC 有什么区别？主要起什么作⽤？ 
- residual leanring 为什么能够提升准确率？ 
- 代码练习⼆⾥，⽹络和1989年 Lecun 提出的 LeNet 有什么区别？ 
- 代码练习⼆⾥，卷积以后feature map 尺⼨会变⼩，如何应⽤ Residual Learning? 
- 有什么⽅法可以进⼀步提升准确率？ 
# 【第一周】深度学习基础

<br>

## 1. **视频学习**

### **1.1 绪论**

- 从专家系统到机器学习
- 从传统机器学习到深度学习
- 深度学习的能与不能

### **1.2 深度学习概述**

- 浅层神经⽹络：⽣物神经元到单层感知器，多层感知器，反向传播和梯度消失
- 神经⽹络到深度学习：逐层预训练，⾃编码器和受限玻尔兹曼机

<br>



## 2. **代码练习**

代码练习需要使⽤⾕歌的 Colab，它是⼀个 Jupyter 笔记本环境，已经默认安装好 pytorch，不需要进⾏任何设置就可以使⽤，并且完全在云端运⾏。使⽤⽅法可以参考 Rogan 的博客：

https://www.cnblogs.com/lfri/p/10471852.html 国内⽬前⽆法访问 colab，可以安装 Ghelper:

http://googlehelper.net/

### **2.1 pytorch 基础练习**

基础练习部分包括 pytorch 基础操作，[实验指导链接](https://gitee.com/gaopursuit/ouc-dl/blob/master/lab/week01_Pytorch_Basic.ipynb)

**要求：** 把代码输⼊ colab，在线运⾏观察效果。

### **2.2 螺旋数据分类**

⽤神经⽹络实现简单数据分类，[实验指导链接](https://gitee.com/gaopursuit/ouc-dl/blob/master/lab/week01_Spiral_Classification.ipynb)

**要求：** 把代码输⼊ colab，在线运⾏观察效果

<br>



## 3. **博客作业**

完成⼀篇博客，博客内容包括两部分：

【第⼀部分：代码练习】在⾕歌 Colab 上完成 pytorch 代码练习中的 2.1 pytorch基础练习、2.2螺旋数据分类，关键步骤截图，并附⼀些⾃⼰的想法和解读。

【第⼆部分：问题总结】思考下⾯的问题：

1、AlexNet有哪些特点？为什么可以⽐LeNet取得更好的性能？ 

2、激活函数有哪些作⽤？ 

3、梯度消失现象是什么？

4、神经⽹络是更宽好还是更深好？

5、为什么要使⽤Softmax? 

6、SGD 和 Adam 哪个更有效？

如果还有其它问题，可以总结⼀下，写在博客⾥，下周⼀起讨论。
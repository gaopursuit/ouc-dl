# 【第五周】生成式对抗网络 & Diffussion

<br>

# 1、视频学习

## 1.1 生成式对抗网络

- GAN，CGAN，DCGAN。WGAN了解一下就可以，不需要掌握。
- 生成对抗网络GAN前沿与实战，了解一下就可以，不需要掌握。
- 学习 GAN，CGAN，DCGAN 的 pytroch 代码实现。

## 1.2 Diffusion 模型

- 从0理解 Diffusion模型-前置知识，【[视频链接](https://www.bilibili.com/video/BV1re4y1m7gb)】
- Diffusion 模型简明综述，【[视频链接](https://www.bilibili.com/video/BV1TP4y1Q7qJ)】
- Diffusion 模型技术前沿与落地应用，【[视频链接](https://www.bilibili.com/video/BV1bzsDe1ET1)】



# 2、代码练习

学习王贯安提供的 GAN, CGAN, DCGAN 代码（ https://github.com/wangguanan/Pytorch-Basic-GANs ），有条件的话跑一跑代码（对GPU资源要求相对较高），体会体会即可。

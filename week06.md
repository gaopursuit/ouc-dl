# 【第六周】Vision Transformer

<br>

# 1、视频学习

下面是我目前在全网找到，讲的最好的 ViT 和 Swin Transformer 的学习材料，大家可以学习：

- Transformer中Self-Attention以及Multi-Head Attention详解 【[视频链接](https://www.bilibili.com/video/BV15v411W78M/)】
- Vision Transformer (ViT) 网络详解 【[视频链接](https://www.bilibili.com/video/BV1Jh411Y7WQ)】

还有一个 视觉 Transformer 前沿讲座，大家可以学习：

- 华为韩凯：视觉 Transformer 综述 【[视频链接](https://www.bilibili.com/video/BV1cr4y1m7tS)】



# 2、代码学习

学习下面的视频，学习如何使用 pytorch 搭建 ViT  和 Swin Transformer

- 使用 pytorch 搭建 Vision Transformer 模型 【[视频链接](https://www.bilibili.com/video/BV1AL411W7dT)】
- 使用 pytorch 搭建 Swin Transformer 模型 【[视频链接](https://www.bilibili.com/video/BV1yg411K7Yc)】


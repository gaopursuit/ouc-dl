# 【第三周】ResNet+ResNeXt

<br>

# 1、论文阅读与视频学习

ResNet 阅读论⽂ *Deep Residual Learning for Image Recognition，CVPR2016* 同时学习下⾯的视频 

● ResNet⽹络讲解： https://www.bilibili.com/video/BV1T7411T7wa/

● Pytorch搭建ResNet⽹络：https://www.bilibili.com/video/BV14E411H7Uw/

ResNeXt 阅读论⽂ *Aggregated Residual Transformations for Deep Neural Networks，* *CVPR* *2017* 同时学习下⾯视频 

● ResNeXt⽹络讲解：https://www.bilibili.com/video/BV1Ap4y1p71v/

● Pytorch搭建ResNeXt⽹络：https://www.bilibili.com/video/BV1rX4y1N7tE/

# 2、代码作业

AI研习社 “猫狗⼤战” ⽐赛（[访问链接](https://god.yanxishe.com/8)），⼤家在学习完 LeNet 以后，可以⽤类似 LetNet 的⽹络结构参加这个⽐赛，结果上传以后会有实时得分。 在此基础上，应⽤ ResNet，看看效果有什么区别。 

![img](https://gaopursuit.oss-cn-beijing.aliyuncs.com/2024/20240628205141.jpg)

本周的思考题：

1、Residual learning 的基本原理？

2、Batch Normailization 的原理，思考 BN、LN、IN 的主要区别。

3、为什么分组卷积可以提升准确率？既然分组卷积可以提升准确率，同时还能降低计算量，分组数量尽量多不⾏吗？

附加思考题： 有余⼒的同学可以学习程明明⽼师的 Res2Net，体会⾥⾯是如何利⽤分组卷积降低 计算量，同时提升⽹络性能的。感兴趣的同学可以学习⼀下 Vision Transformer ⾥的 attention， ⽐较 multi-head 和 分组卷积 的区别与联系。 
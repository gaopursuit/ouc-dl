![header](note/assets/微信截图_20240710230929.jpg)



欢迎来到中国海洋大学人工智能研究院 2024 年深度学习入门课程，请关注后续信息的更新。 **（本课程用于2024年暑期实验室新生教学）** 

感兴趣的同学可以加入课堂，<font color="red">自愿参加各个学习环节、自愿提交作业</font>

<br>

# 1、课程大纲与相关信息

视频资料： 可以在 [网易云课堂](https://study.163.com/course/introduction/1006498024.htm) 获取，目前价格只要0.1元，非常划算。主要内容包括DNN、CNN、RNN/LSTM、GAN以及强化学习等，应用实例包括计算机视觉的图像分类、目标检测、图像生成等，帮助学员了解、理解、掌握深度学习的基础和前沿算法，并拥有深度学习算法实战技能。

另外，也可以参考丛润民老师的《[深度学习课程](https://rmcong.github.io/proj_deep_learning_ProfessionalCourse.html)》

每周安排（可能会有临时性的变化）：

-  周一，学生根据提供的相关材料开始学习本周内容；
-  周五，把作业发给导师，自愿在雨课堂提交，便于了解进度，帮助还在挠头的同学；
-  周日，基于作业初稿，老师组织进行相关讨论，有可能增加相关的讲座或教程。

<br>



# 2、教学内容

### 🚩【第一周】 [深度学习基础](./week01.md)  

**主要内容：** 浅层神经⽹络、⽣物神经元到单层感知器，多层感知器，反向传播和梯度消失、神经⽹络到深度学习：逐层预训练，⾃编码器。

扩展阅读材料：【[PyTorch代码技巧](https://gitee.com/gaopursuit/ouc-dl/blob/master/note/01-PyTorch%E4%BB%A3%E7%A0%81%E6%8A%80%E5%B7%A7.md)】

<br>

### 🚩【第二周】 [卷积神经网络](./week02.md)

 **主要内容：** CNN的基本结构：卷积、池化、全连接。典型的⽹络结构：AlexNet、VGG、GoogleNet、ResNet

<br>

### 🚩【第三周】 [ResNet+ResNeXt](./week03.md)

**主要内容：** ResNet 和 ResNeXt，完成猫狗大战编程练习。

<br>

### 🚩【第四周】 [MobileNet_ShuffleNet](./week04.md)

**主要内容：** 学习MobileNet, ShuffleNet,  学习 SENet 和 CBAM，完成 HyrbridSN 编程练习。

<br>

### 🚩【第五周】 [ 生成式对抗网络 & Diffusion  ](./week05.md)

**主要内容：** 生成式对抗网络基础，前沿与实战。Diffusion 模型。

<br>

### 🚩【第六周】 [视觉Transformer 前沿 ](./week06.md)

**主要内容：** Self-Attention以及 Multi-head Attention 机制，Vision Transformer，Swin Transformer，视觉Transformer综述， 以及如何使用 pytorch 搭建 Vision Transformer

<br>




# 3、相关资料

- 《深度学习：算法到实战》，网易云课堂 [[链接]](https://study.163.com/course/introduction/1006498024.htm)
- 《神经网络与深度学习》，复旦大学 邱锡鹏著 [[下载]](https://rmcong.github.io/DLCourse/book1.pdf)
- 《深度学习(PyTorch)》课程视频及ppt，纽约大学 Yann Lecun [[链接]](https://atcold.github.io/didactics.html)
- 《Convolutional Neural Networks for Visual Recognition》，斯坦福大学 Fei-Fei Li (李飞飞) [[链接]](http://cs231n.stanford.edu/)

大家有问题欢迎随时邮件联系我：gaofeng@ouc.edu.cn，欢迎大家多多提出意见！

 